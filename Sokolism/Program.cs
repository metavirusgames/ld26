﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokolism {
    public class Program {

        public static void Main ( string[] args ) {

            using ( SokolismGame game = new SokolismGame() ) {
                game.Run();
            }
        }
    }
}
