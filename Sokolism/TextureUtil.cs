﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokolism {
    public sealed class TextureUtil {

        public static Texture2D Load (GraphicsDevice graphicsDevice, string filename ) {
            if ( File.Exists( filename ) ) {
                FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

                Texture2D result = Texture2D.FromStream( graphicsDevice, fileStream );

                fileStream.Close();
                fileStream.Dispose();

                return result;
            } else {
                throw new FileNotFoundException();
            }
        }
    }
}
