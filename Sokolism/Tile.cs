﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokolism {
    public class Tile {

        public static int WIDTH = 32;
        public static int HEIGHT = 32;

        public int TileIndex { get; private set; }

        public Collision Collision { get; private set; }

        public Tile ( int tileIndex, Collision collision ) {
            this.TileIndex = tileIndex;
            Collision = collision;
        }

    }
}
