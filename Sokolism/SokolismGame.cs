﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokolism {
    public sealed class SokolismGame : Game {

        public GraphicsDeviceManager Graphics { get; private set; }

        public SpriteBatch SpriteBatch { get; private set; }

        private Level _level;

        private int _currentLevel = 1;
        private int _maxLevels = 0;
        private bool _won = false;

        public SokolismGame () {

            this.Graphics = new GraphicsDeviceManager( this ) { PreferredBackBufferWidth = 800, PreferredBackBufferHeight = 600 };

            this.Content.RootDirectory = "Data";
        }

        protected override void Initialize () {
            this.SpriteBatch = new SpriteBatch( this.GraphicsDevice );

            Fonts.Load( Content );

            if ( File.Exists( @"_currentLevel.dat" ) ) {
                string lines = File.ReadAllText( @"_currentLevel.dat" );

                _currentLevel = int.Parse( lines );
            }

            ChangeLevel( String.Format(@"Data/Levels/{0}.png", _currentLevel) );

            DirectoryInfo info = new DirectoryInfo( @"Data/Levels/" );

            FileInfo[] files = info.GetFiles();

            _maxLevels = files.Count();

            

            base.Initialize();
        }

        protected override void Update ( GameTime gameTime ) {

            if ( !_won ) {
                _level.Update( gameTime );
            } else {

            }

            base.Update( gameTime );
        }

        protected override void Draw ( GameTime gameTime ) {

            this.GraphicsDevice.Clear( Color.Black );

            this.SpriteBatch.Begin();

            _level.Draw( gameTime, SpriteBatch );

            if ( _won ) {
                string wonString = "You Have Won!";
                Vector2 wonStringSize = Fonts.Andy36.MeasureString(wonString);
                this.SpriteBatch.DrawString( Fonts.Andy36, wonString, new Vector2( 400 - wonStringSize.X / 2, 300 - wonStringSize.Y / 2 ) + new Vector2(1, 1), Color.Black );
                this.SpriteBatch.DrawString( Fonts.Andy36, wonString, new Vector2( 400 - wonStringSize.X / 2, 300 - wonStringSize.Y / 2 ), Color.White );
            }

            //this.SpriteBatch.DrawString( Fonts.Andy14, "Steps: " + _level.Steps, new Vector2( 5, 5 ), Color.White );

            this.SpriteBatch.End();

            base.Draw( gameTime );
        }

        public void RestartLevel () {
            ChangeLevel( String.Format( @"Data/Levels/{0}.png", _currentLevel ) );
        }

        public void NextLevel () {

            if ( _currentLevel + 1 <= _maxLevels ) {
                _currentLevel += 1;
                ChangeLevel( String.Format( @"Data/Levels/{0}.png", _currentLevel ) );

                using ( StreamWriter writer = new StreamWriter( @"_currentLevel.dat" ) ) {
                    writer.Write( _currentLevel.ToString() );
                }

            } else {
                _won = true;   
            }
            
        }

        public void ChangeLevel ( string filename ) {
            _level = new Level( this, TextureUtil.Load( this.GraphicsDevice, @"Data/Textures/Tileset.png" ), TextureUtil.Load( this.GraphicsDevice, filename ) );
        }

    }
}
