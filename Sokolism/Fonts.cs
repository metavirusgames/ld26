﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokolism {
    public sealed class Fonts {

        public static SpriteFont Andy36 { get; private set; }

        public static SpriteFont Andy14 { get; private set; }

        public static void Load (ContentManager contentManager) {

            Andy36 = contentManager.Load<SpriteFont>( @"Fonts/Andy36" );

            Andy14 = contentManager.Load<SpriteFont>( @"Fonts/Andy14" );

        }
    }
}
