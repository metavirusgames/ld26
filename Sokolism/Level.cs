﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokolism {
    public sealed class Level {

        public static int Width = 25;
        public static int Height = 19;


        public Tile[,] Data;

        public Point Player { get; private set; }

        //public Point Rock { get; private set; }

        //public Point Exit { get; private set; }

        public List<Point> Exits { get; private set; }

        public List<Point> Rocks { get; private set; }

        public Texture2D Tileset { get; private set; }

        public Texture2D LevelTexture { get; private set; }

        public Rectangle[] Tiles { get; private set; }

        public Tile RockTile = new Tile( 2, Collision.None );

        public Tile PlayerTile = new Tile( 8, Collision.None );

        public int Steps { get; private set; }

        private KeyboardState _previousKeyboardState;

        private bool _waitingToContinue;

        private SokolismGame _game;

        public Level ( SokolismGame game, Texture2D tileset, Texture2D levelTexture ) {
            this._game = game;
            this.Tileset = tileset;
            this.LevelTexture = levelTexture;


            Initialize();
        }

        public void Initialize () {

            this.Steps = 0;
            this.Rocks = new List<Point>();
            this.Exits = new List<Point>();

            Data = new Tile[Level.Width, Level.Height];
            Tiles = new Rectangle[this.Tileset.Width / Tile.WIDTH * this.Tileset.Height / Tile.HEIGHT];

            int j = 0;
            for ( int y = 0; y < this.Tileset.Height / Tile.HEIGHT; y++ ) {
                for ( int x = 0; x < this.Tileset.Width / Tile.WIDTH; x++ ) {
                    Tiles[j] = new Rectangle( x * Tile.WIDTH, y * Tile.HEIGHT, Tile.WIDTH, Tile.HEIGHT );
                    j++;
                }
            }

            Color[] colors = new Color[this.LevelTexture.Width * this.LevelTexture.Height];

            this.LevelTexture.GetData<Color>( colors );

            int i = 0;
            for ( int y = 0; y < Level.Height; y++ ) {
                for ( int x = 0; x < Level.Width; x++ ) {
                    Color tileColor = colors[i];

                    if ( tileColor != new Color( 0, 0, 0, 0 ) ) {
                        if ( tileColor == Color.Black ) {
                            Data[x, y] = new Tile( 1, Collision.Impassable );
                        } else if ( tileColor == new Color( 0, 255, 0 ) ) {
                            Exits.Add( new Point( x, y ) );
                            //Exit = new Point( x, y );
                            Data[x, y] = new Tile( 3, Collision.Floor );
                        } else if ( tileColor == Color.Red ) {
                            Player = new Point( x, y );
                            Data[x, y] = new Tile( 5, Collision.Floor );
                        } else if ( tileColor == new Color( 79, 79, 79 ) ) {
                            Rocks.Add( new Point( x, y ) );
                            //Rock = new Point( x, y );
                            Data[x, y] = new Tile( 5, Collision.Floor );
                        } else {
                            Data[x, y] = new Tile( 5, Collision.Floor );
                        }
                    }

                    i++;
                }
            }
        }

        public void Update ( GameTime gameTime ) {
            KeyboardState keyboard = Keyboard.GetState();

            if ( !_waitingToContinue ) {
                if ( keyboard.IsKeyDown( Keys.Up ) && !_previousKeyboardState.IsKeyDown( Keys.Up ) ) {
                    if ( IsFloor( Player.X, Player.Y - 1 ) && !IsRock( Player.X, Player.Y - 1 ) ) {
                        Player = new Point( Player.X, Player.Y - 1 );
                        Steps += 1;
                    } else if ( IsFloor( Player.X, Player.Y - 1 ) && IsRock( Player.X, Player.Y - 1 ) ) {

                        Point rock = GetRock( Player.X, Player.Y - 1 );
                        if ( IsFloor( rock.X, rock.Y - 1 ) ) {

                            if ( !IsRock( rock.X, rock.Y - 1 ) ) {
                                Player = new Point( Player.X, Player.Y - 1 );
                                Steps += 1;
                                MoveRock( rock, new Point( rock.X, rock.Y - 1 ) );
                            }
                        }

                    }
                }

                if ( keyboard.IsKeyDown( Keys.Down ) && !_previousKeyboardState.IsKeyDown( Keys.Down ) ) {
                    if ( IsFloor( Player.X, Player.Y + 1 ) && !IsRock( Player.X, Player.Y + 1 ) ) {
                        Player = new Point( Player.X, Player.Y + 1 );
                        Steps += 1;
                    } else if ( IsFloor( Player.X, Player.Y + 1 ) && IsRock( Player.X, Player.Y + 1 ) ) {

                        Point rock = GetRock( Player.X, Player.Y + 1 );
                        if ( IsFloor( rock.X, rock.Y + 1 ) ) {

                            if ( !IsRock( rock.X, rock.Y + 1 ) ) {
                                Player = new Point( Player.X, Player.Y + 1 );
                                Steps += 1;
                                MoveRock( rock, new Point( rock.X, rock.Y + 1 ) );
                            }
                        }
                    }
                }

                if ( keyboard.IsKeyDown( Keys.Left ) && !_previousKeyboardState.IsKeyDown( Keys.Left ) ) {
                    if ( IsFloor( Player.X - 1, Player.Y ) && !IsRock( Player.X - 1, Player.Y ) ) {
                        Player = new Point( Player.X - 1, Player.Y );
                        Steps += 1;
                    } else if ( IsFloor( Player.X - 1, Player.Y ) && IsRock( Player.X - 1, Player.Y ) ) {

                        Point rock = GetRock( Player.X - 1, Player.Y );
                        if ( IsFloor( rock.X - 1, rock.Y ) ) {

                            if ( !IsRock( rock.X - 1, rock.Y ) ) {
                                Player = new Point( Player.X - 1, Player.Y );
                                Steps += 1;
                                MoveRock( rock, new Point( rock.X - 1, rock.Y ) );
                            }
                        }
                    }
                }

                if ( keyboard.IsKeyDown( Keys.Right ) && !_previousKeyboardState.IsKeyDown( Keys.Right ) ) {
                    if ( IsFloor( Player.X + 1, Player.Y ) && !IsRock( Player.X + 1, Player.Y ) ) {
                        Player = new Point( Player.X + 1, Player.Y );
                        Steps += 1;
                    } else if ( IsFloor( Player.X + 1, Player.Y ) && IsRock( Player.X + 1, Player.Y ) ) {

                        Point rock = GetRock( Player.X + 1, Player.Y );
                        if ( IsFloor( rock.X + 1, rock.Y ) ) {

                            if ( !IsRock( rock.X + 1, rock.Y ) ) {
                                Player = new Point( Player.X + 1, Player.Y );
                                Steps += 1;
                                MoveRock( rock, new Point( rock.X + 1, rock.Y ) );
                            }
                        }
                    }
                }

                if ( keyboard.IsKeyDown( Keys.R ) && !_previousKeyboardState.IsKeyDown( Keys.R ) ) {
                    _game.RestartLevel();
                }

                if ( GetAvailableExits() == 0 ) {
                    _waitingToContinue = true;
                }
            } else {

                if ( keyboard.IsKeyDown( Keys.X ) && !_previousKeyboardState.IsKeyDown( Keys.X ) ) {
                    _waitingToContinue = false;
                    _game.NextLevel();
                }
            }

            _previousKeyboardState = keyboard;
        }

        public void Draw ( GameTime gameTime, SpriteBatch spriteBatch ) {

            for ( int y = 0; y < Level.Height; y++ ) {
                for ( int x = 0; x < Level.Width; x++ ) {

                    if ( Data[x, y] != null ) {

                        spriteBatch.Draw( Tileset, new Vector2( x * Tile.WIDTH, y * Tile.HEIGHT ), Tiles[Data[x, y].TileIndex], Color.White );

                    }
                }
            }

            foreach ( Point rock in Rocks ) {
                spriteBatch.Draw( Tileset, new Vector2( rock.X * Tile.WIDTH, rock.Y * Tile.HEIGHT ), Tiles[RockTile.TileIndex], Color.White );
            }

            spriteBatch.Draw( Tileset, new Vector2( Player.X * Tile.WIDTH, Player.Y * Tile.HEIGHT ), Tiles[PlayerTile.TileIndex], Color.White );

            if ( _waitingToContinue ) {
                string winString = "Level Complete! Press X To Continue!";
                Vector2 stringSize = Fonts.Andy36.MeasureString( winString );

                spriteBatch.DrawString( Fonts.Andy36, winString, new Vector2( 400 - stringSize.X / 2, 300 - stringSize.Y / 2 ) + new Vector2( 1, 1 ), Color.Black );
                spriteBatch.DrawString( Fonts.Andy36, winString, new Vector2( 400 - stringSize.X / 2, 300 - stringSize.Y / 2 ), Color.White );
            }
        }

        public bool IsFloor ( int x, int y ) {
            return Data[x, y].Collision == Collision.Floor;
        }

        public void MoveRock ( Point rockLocation, Point newRockLocation ) {

            for ( int i = 0; i < Rocks.Count; i++ ) {
                if ( rockLocation.X == Rocks[i].X && rockLocation.Y == Rocks[i].Y ) {
                    Rocks[i] = newRockLocation;
                }
            }
        }

        public Point GetRock ( int x, int y ) {

            foreach ( Point rock in Rocks ) {
                if ( x == rock.X && y == rock.Y ) {
                    return rock;
                }
            }

            return Point.Zero;
        }

        public bool IsRock ( int x, int y ) {

            foreach ( Point rock in Rocks ) {
                if ( x == rock.X && y == rock.Y ) {
                    return true;
                }
            }

            return false;
        }

        public int GetAvailableExits () {
            int i = Exits.Count;

            foreach ( Point exit in Exits ) {

                IEnumerable<Point> occupiedExits = Rocks.Where( r => r.X == exit.X && r.Y == exit.Y );

                if ( occupiedExits.Count() > 0 ) {
                    i -= 1;
                }
            }

            return i;
        }

        public bool IsEmpty ( int[,] data, int x, int y ) {
            return data[x, y] == 0;
        }
    }
}
